// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.score;

import de.docm77.patreon.o2tower.Room;
import de.docm77.patreon.o2tower.event.RoomEventHandler;

import java.util.HashSet;

import org.bukkit.World;
import org.bukkit.entity.Player;

import de.docm77.patreon.o2tower.Config;
import de.docm77.patreon.o2tower.IGame;
import de.docm77.patreon.o2tower.O2Tower;
import de.docm77.patreon.o2tower.utils.ScoreManager;


public class PlayerScoreManager implements RoomEventHandler, IGame
{
  private HashSet<Player> players = new HashSet<>();
  final private ScoreManager scoreMgr;
  
  final private int startScore;
  final private Room room;

  public PlayerScoreManager(Config cfg, World world)
  {
    scoreMgr = O2Tower.instance().getScoreManager();
    startScore = cfg.getValue("startScore", Integer.class, 0);

    scoreMgr.showMainScore();

    Room.Config rcfg = O2Tower.instance().config().getRoomConfig("maingame", world);
    room = new Room(O2Tower.instance(), rcfg, "maingame");
    room.eventHandler = this;
  }

  public void addNewPlayer(Player player)
  {
    this.room.tagPlayer(player, true);
  }

  private void removePlayer(Player player)
  {
    if(players.contains(player))
    {
      players.remove(player);
    }
  }

  @Override
  public void tick()
  {
    for(Player p : players)
    {
      scoreMgr.substractFromMainScore(p, 1, false);
    }
  }

  @Override
  public void playerEntered(Player player) {
    if(!players.contains(player))
    {
      scoreMgr.setMainScore(player, startScore);
      players.add(player);
    }
  }

  @Override
  public void playerLeft(Player player) {
    removePlayer(player);
  }

  @Override
  public Room getRoom() {
    return room;
  }

}
