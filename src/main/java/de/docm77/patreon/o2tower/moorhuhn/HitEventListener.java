// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.moorhuhn;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class HitEventListener implements Listener
{
  final private Game parent;

  public HitEventListener(Game parent)
  {
    this.parent = parent;
  }

  @EventHandler
  public void onHitEvent(ProjectileHitEvent event)
  {
    Entity hitEntity = event.getHitEntity();
    Projectile projectile = event.getEntity();
    Player shooter = (Player) projectile.getShooter();
    if(hitEntity == null)
    {
      return;
    }
    if(hitEntity.getType() != EntityType.CHICKEN)
    {
      return;
    }
    if(projectile.getType() != EntityType.SNOWBALL)
    {
      return;
    }
    if(shooter == null)
    {
      return;
    }

    parent.entityHit(shooter, hitEntity);
  }
}
