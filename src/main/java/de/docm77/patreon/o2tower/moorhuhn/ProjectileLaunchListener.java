// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.moorhuhn;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;

public class ProjectileLaunchListener implements Listener {
  final private de.docm77.patreon.o2tower.moorhuhn.Game parent;

  public ProjectileLaunchListener(de.docm77.patreon.o2tower.moorhuhn.Game parent)
  {
    this.parent = parent;
  }

  @EventHandler
  void onLaunch(ProjectileLaunchEvent event)
  {
    if(!(event.getEntity() instanceof Snowball))
    {
      return;
    }
    Snowball projectile = (Snowball) event.getEntity();
    if(!(projectile.getShooter() instanceof Player))
    {
      return;
    }
    parent.refillSnowballs();
    Player shooter = (Player) projectile.getShooter();
    if(shooter.getInventory().getItemInMainHand().getAmount() < 10) {
      shooter.getInventory().addItem(new ItemStack(Material.SNOWBALL));
    }
//        shooter.getInventory().setItemInMainHand(new ItemStack(Material.SNOWBALL, 16));
  }

}
