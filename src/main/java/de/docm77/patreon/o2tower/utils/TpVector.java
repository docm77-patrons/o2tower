// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.utils;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class TpVector extends Vector {
  public enum Direction
  {
    North, East, South, West
  }
  private Direction facing = null;

  public TpVector()
  {
    super();
    this.facing = null;
  }

  public TpVector(Vector vec, Direction facing)
  {
    super(vec.getX(), vec.getY(), vec.getZ());
    this.facing = facing;
  }

  public TpVector(double x, double y, double z, Direction facing)
  {
    super(x,y,z);
    this.facing = facing;
  }

  public float getFacingRotation(Player player)
  {
    if(this.facing != null)
    {
      switch(this.facing)
      {
        case South: return 0.0f;
        case West: return 90.0f;
        case East: return -90.0f;
        case North: return -180.0f;
      }
    }
    return player==null ? 0.0f : player.getLocation().getYaw();
  }

  public float getFacingRotation()
  {
    return this.getFacingRotation(null);
  }

}
