// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.dropper;

import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.docm77.patreon.o2tower.IGame;
import de.docm77.patreon.o2tower.O2Tower;
import de.docm77.patreon.o2tower.Room;
import de.docm77.patreon.o2tower.event.RoomEventHandler;
import de.docm77.patreon.o2tower.utils.TpVector;
import de.docm77.patreon.o2tower.utils.Utils;

public class Game implements RoomEventHandler, IGame
{
  final private O2Tower parent;
  final private World world;
  final private Room room;

  final HashSet<Player> players = new HashSet<>();

  // private final Location tp_to;
  final private TpVector tp_to;


  public Game(O2Tower parent, World world)
  {
    this.parent = parent;
    this.world = world;

    this.tp_to = this.parent.config().getVectorValue("dropper.tp_if_failed", new Vector(0,0,0));

    Room.Config rcfg = this.parent.config().getRoomConfig("dropper", this.world);
    room = new Room(this.parent, rcfg, "dropper");
    room.eventHandler = this;
    
  }

  @Override
  public Room getRoom()
  {
    return room;
  }

  @Override
  public void tick()
  {
    for(Player player : this.room.playersInRoom())
    {
      // parent.getLogger().info("" + player.getVelocity().getY() + ", " +( player.isFlying() ? "flying" : "not flying") + (player.isOnGround() ? "on ground" : "not on ground"));
      if(player.isOnGround())
      {
        room.tagPlayer(player, false);
        boolean wrongLocation = true;
        for(Location loc : room.getConfig().exit)
        {
          if(Utils.hasLocation(player.getLocation(), loc))
          {
            wrongLocation = false;
            break;
          }
        }
        if(wrongLocation)
        {
          Location location = Utils.vecToLocation(this.tp_to, world);
          location.setYaw(this.tp_to.getFacingRotation(player));
          player.teleport(location);
        }
      }
    }
  }

  @Override
  public void playerEntered(Player player) {
    // TODO Auto-generated method stub
    this.players.add(player);
    
  }

  @Override
  public void playerLeft(Player player) {
    // TODO Auto-generated method stub
    this.players.remove(player);
    
  }  
}
