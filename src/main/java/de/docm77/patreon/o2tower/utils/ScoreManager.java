// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.utils;


import de.docm77.patreon.o2tower.Config;
import de.docm77.patreon.o2tower.O2Tower;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.Objects;

public class ScoreManager
{
  private final O2Tower parent;
  private final Config cfg;
  private final Objective mainObjective;
  private final Scoreboard scoreboard;
  private final String dummyName = "_counterdummy_";

  public ScoreManager(O2Tower parent)
  {
    this.parent = parent;
    this.cfg = parent.config();
    scoreboard = Objects.requireNonNull(parent.getServer().getScoreboardManager()).getMainScoreboard();
    Objective obj = scoreboard.getObjective(cfg.getObjectiveName());
    if(obj == null)
    {
      obj = scoreboard.registerNewObjective(cfg.getObjectiveName(), "dummy", cfg.getObjectiveName());
    }
    mainObjective = obj;
  }

  public Objective getAndRegisterObjective(String name)
  {
    Objective obj = scoreboard.getObjective(name);
    if(obj == null)
    {
      obj = scoreboard.registerNewObjective(name, "dummy", name);
    }
    return obj;
  }

  public int addToScore(Objective objective, Player player, int delta)
  {
    Score score = objective.getScore(player.getDisplayName());
    score.setScore(score.getScore() + delta);
    return score.getScore();
  }
  
  public int substractFromScore(Objective objective, Player player, int delta, boolean allowNegative)
  {
    Score score = objective.getScore(player.getDisplayName());
    int s = score.getScore() - delta;
    score.setScore(allowNegative ? s : Math.max(s, 0));
    return score.getScore();
  }
  
  public void setScore(Objective objective, Player player, int points)
  {
    Score score = objective.getScore(player.getDisplayName());
    score.setScore(points);
  }

  public void addToMainScore(Player player, int delta)
  {
    addToScore(mainObjective, player, delta);
  }
  
  public void substractFromMainScore(Player player, int delta, boolean allowNegative)
  {
    substractFromScore(mainObjective, player, delta, allowNegative);
  }
  
  public void setMainScore(Player player, int points)
  {
    setScore(mainObjective, player, points);
  }

  public void showMainScore()
  {
    mainObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
  }

  public void hideMainScore()
  {
    mainObjective.setDisplaySlot(null);
  }

}
