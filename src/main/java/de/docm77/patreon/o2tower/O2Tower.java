// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower;

import de.docm77.patreon.o2tower.commands.Stuck;
import de.docm77.patreon.o2tower.event.LocationListener;
import de.docm77.patreon.o2tower.score.PlayerScoreManager;
import de.docm77.patreon.o2tower.utils.ScoreManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public final class O2Tower extends JavaPlugin implements Listener
{
  private Config cfg = null;
  private ScoreManager scoreManager = null;
  private LocationListener locationListener;
  private Stuck stuckCmd;
  private final ArrayList<Room> cachedGameRooms = new ArrayList<>();

  private de.docm77.patreon.o2tower.moorhuhn.Game moorhuhnGame;
  private de.docm77.patreon.o2tower.dropper.Game dropperGame;
  private PlayerScoreManager playerScoreManager;
  
  static private O2Tower _instance = null;

  public static O2Tower instance()
  {
    return _instance;
  }


  @Override
  public void onEnable() {
    O2Tower._instance = this;
    this.saveDefaultConfig();
    cfg = new Config(this);
    cfg.load();

    scoreManager = new ScoreManager(this);

    World world = getServer().getWorlds().get(0);
    moorhuhnGame = new de.docm77.patreon.o2tower.moorhuhn.Game(this, world);
    dropperGame = new de.docm77.patreon.o2tower.dropper.Game(this, world);
    playerScoreManager = new PlayerScoreManager(cfg, world);

    locationListener = new LocationListener(this);

    stuckCmd = new Stuck(cfg, world);
    this.getCommand("stuck").setExecutor(stuckCmd);

    cachedGameRooms.add(moorhuhnGame.getRoom());
    cachedGameRooms.add(dropperGame.getRoom());
    cachedGameRooms.add(playerScoreManager.getRoom());

    
    getServer().getPluginManager().registerEvents(this, this);

    
    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
      public void run()
      {
        moorhuhnGame.tick();
        dropperGame.tick();
      }
    }, 20*10, 1);

    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
      public void run()
      {
        playerScoreManager.tick();
      }
    }, 20, 20);

    getLogger().info("O2 Tower loaded");
    
  }

  @Override
  public void onDisable() {
    moorhuhnGame.stop();
  }

  @EventHandler
  public void playerLogin(PlayerLoginEvent event)
  {
    moorhuhnGame.start();
  }

  // @EventHandler
  // public void onWorldLoaded(WorldLoadEvent event)
  // {
  //   getLogger().info("World " + event.getWorld().getName() + " loaded");
  //   if(event.getWorld() == getServer().getWorlds().get(0))
  //   {
  //     moorhuhnGame.start();
  //   }
  // }

  public List<Room> getRooms()
  {
    return cachedGameRooms;
  }

  public Config config() { return cfg; }

  public void reloadTheConfig() {
    reloadConfig();
    cfg.load();
  }

  public ScoreManager getScoreManager() {
    return scoreManager;
  }
}
