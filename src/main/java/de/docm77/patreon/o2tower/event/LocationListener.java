// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.event;

import de.docm77.patreon.o2tower.O2Tower;
import de.docm77.patreon.o2tower.Room;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class LocationListener implements Listener {
  final private O2Tower parent;

  public LocationListener(O2Tower parent)
  {
    this.parent = parent;
    this.parent.getServer().getPluginManager().registerEvents(this, this.parent);
  }

  @EventHandler
  public void onMove(org.bukkit.event.player.PlayerMoveEvent event)
  {
//        System.out.println("Player moved" + event.getTo());
    for(Room room : parent.getRooms())
    {
      room.checkPlayer(event.getPlayer(), event.getTo(), event.getFrom());
    }
//        event.
  }
}
