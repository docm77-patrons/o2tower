// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower;

import de.docm77.patreon.o2tower.event.RoomEventHandler;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Room
{
  public static class Config
  {
    public String playerTag = "--none--";
    public List<Location> entry = new ArrayList<>();
    public List<Location> exit = new ArrayList<>();
  }

  final private Config cfg;

  final private O2Tower parent;
  final private String friendlyName;

  public RoomEventHandler eventHandler = null;
  public boolean hasBossBar = true;

  public Room(O2Tower parent, Config cfg, String friendlyName)
  {
    this.parent = parent;
    this.cfg = cfg;
    this.friendlyName = friendlyName;
    this.parent.getLogger().info("Creating new room");
  }

  public Config getConfig()
  {
    return cfg;
  }

  public boolean inRoom(Player player)
  {
    if(cfg == null) {
      return false;
    }
    return player.getScoreboardTags().contains(cfg.playerTag);
  }

  public HashSet<Player> playersInRoom()
  {
    HashSet<Player> res = new HashSet<>();
    for(Player p : this.parent.getServer().getOnlinePlayers())
    {
      if(p.getScoreboardTags().contains(cfg.playerTag)) {
        res.add(p);
      }
    }
    return res;
  }



  private boolean hasEntered(Location location)
  {
    if(cfg == null) {
      return false;
    }
    if(cfg.entry.size() == 0)
    {
      return false;
    }
    for(Location coord : cfg.entry)
    {
      double x = Math.floor(location.getX());
      double y = Math.floor(location.getY());
      double z = Math.floor(location.getZ());
      if(
        (x == coord.getX() || x+1 == coord.getX()) &&
        (y == coord.getY() || y+1 == coord.getY()) &&
        (z == coord.getZ() || z+1 == coord.getZ())
        )
      {
        return true;
      }
    }
    return false;
  }

  
  private boolean hasLeft(Location location)
  {
    if(cfg == null) {
      return false;
    }
    if(cfg.exit.size() == 0)
    {
      return false;
    }
    for(Location coord : cfg.exit)
    {
      double x = Math.floor(location.getX());
      double y = Math.floor(location.getY());
      double z = Math.floor(location.getZ());
      if(
        (x == coord.getX() || x+1 == coord.getX()) &&
        (y == coord.getY() || y+1 == coord.getY()) &&
        (z == coord.getZ() || z+1 == coord.getZ())
        )
      {
        return true;
      }
    }
    return false;
  }

  public void tagPlayer(Player player, boolean inRoom)
  {
    if(inRoom) {
      player.addScoreboardTag(cfg.playerTag);
      if(eventHandler != null)
      {
        eventHandler.playerEntered(player);
      }
    } else {      
      player.removeScoreboardTag(cfg.playerTag);
      if(eventHandler != null)
      {
        eventHandler.playerLeft(player);
      }
    }
  }

  public void checkPlayer(Player player, Location newLocation, Location oldLocation)
  {
    if(cfg == null) {
      return;
    }
    if(!inRoom(player)) {
      if(hasEntered(newLocation))
      {
        System.out.println(player.getDisplayName() + " has entered room " + friendlyName);

        player.addScoreboardTag(cfg.playerTag);
        if(eventHandler != null)
        {
          eventHandler.playerEntered(player);
        }
      }
    } else {
      if(hasLeft(newLocation))
      {
        System.out.println(player.getDisplayName() + " has left room " + friendlyName);

        player.removeScoreboardTag(cfg.playerTag);
        if(eventHandler != null)
        {
          eventHandler.playerLeft(player);
        }
      }
    }
  }

}
