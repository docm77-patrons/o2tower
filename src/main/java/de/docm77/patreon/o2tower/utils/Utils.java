// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.utils;

import org.bukkit.Location;
import org.bukkit.World;
import java.util.logging.Logger;

import de.docm77.patreon.o2tower.O2Tower;

public class Utils
{
  public static TpVector strToVector(String value)
  {
    String[] tmp = value.split(",");
    Logger log = O2Tower.instance().getLogger();
    try {
      double x = tmp[0].equals("*") ? -1024 : Double.parseDouble(tmp[0]);
      double y = tmp[1].equals("*") ? -1024 : Double.parseDouble(tmp[1]);
      double z = tmp[2].equals("*") ? -1024 : Double.parseDouble(tmp[2]);
      String direction_str = tmp.length > 3 ? tmp[3] : null;
      TpVector.Direction direction = null;
      if(direction_str != null)
      {
        if(direction_str.equalsIgnoreCase("n")) { direction = TpVector.Direction.North; }
        else if(direction_str.equalsIgnoreCase("e")) { direction = TpVector.Direction.East; }
        else if(direction_str.equalsIgnoreCase("s")) { direction = TpVector.Direction.South; }
        else if(direction_str.equalsIgnoreCase("w")) { direction = TpVector.Direction.West; }
      }
      return new TpVector(x,y,z, direction);
    }catch (NumberFormatException e) {
      log.warning("Unable to convert " + value + " to vector");
      return null;
    }
  }

  public static Location vecToLocation(TpVector vec, World world)
  {
    Location result = new Location(world, vec.getX(), vec.getY(), vec.getZ());
    result.setYaw((float) vec.getFacingRotation());
    return result;
    // return new Location(world, vec.getX(), vec.getY(), vec.getZ());
  }

  public static boolean hasLocation(Location entityLocation, Location toCheck, double yTolerance)
  {
    double ex = Math.round(entityLocation.getX());
    double ey = Math.round(entityLocation.getY());
    double ez = Math.round(entityLocation.getZ());
    double tx = Math.round(toCheck.getX());
    double ty = Math.round(toCheck.getY());
    double tz = Math.round(toCheck.getZ());

    boolean res = true;
    res = res && (tx>-1024 ? ex==tx : true);
    res = res && (tz>-1024 ? ez==tz : true);
    res = res && (ey-yTolerance<=ty && ey+yTolerance>=ty);
    return res;
  }

  public static boolean hasLocation(Location entityLocation, Location toCheck)
  {
    return hasLocation(entityLocation, toCheck, 0.5);
  }
}
