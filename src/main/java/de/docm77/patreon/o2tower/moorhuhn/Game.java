// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.moorhuhn;

import de.docm77.patreon.o2tower.IGame;
import de.docm77.patreon.o2tower.O2Tower;
import de.docm77.patreon.o2tower.Room;
import de.docm77.patreon.o2tower.event.RoomEventHandler;
import de.docm77.patreon.o2tower.utils.Utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Objective;
import org.bukkit.util.Vector;

import java.util.*;

public class Game implements RoomEventHandler, IGame
{
  final private O2Tower parent;
  final private World world;
  final private Room room;

  final HashSet<Player> players = new HashSet<>();

  private HashSet<Location> spawningSpaces = new HashSet<>();
  private HashSet<Location> despawningSpaces = new HashSet<>();
  private final HashMap<UUID, Chicken> chickens = new HashMap<>();
  private final HashMap<UUID, KeyedBossBar> bossbars = new HashMap<>();
  private final Location tp_to;

  private int maxChickens = 10;
  private int neededHits = 10;
  final public static Random rand = new Random();
  private boolean run = false;

  final private HitEventListener hitEventListener;
  final private ProjectileLaunchListener projectileLaunchListener;

  Objective scoreboardObjective = null;

  final private boolean enabled;


  public Game(O2Tower parent, World world)
  {
    this.parent = parent;
    this.world = world;
    this.scoreboardObjective = parent.getScoreManager().getAndRegisterObjective("moorhuhn");

    hitEventListener = new HitEventListener(this);
    projectileLaunchListener = new ProjectileLaunchListener(this);
    parent.getServer().getPluginManager().registerEvents(hitEventListener, parent);
    parent.getServer().getPluginManager().registerEvents(projectileLaunchListener, parent);

    this.maxChickens = this.parent.config().getValue("moorhuhn.max_chickens", Integer.class, 10);
    this.neededHits = this.parent.config().getValue("moorhuhn.needed_hits", Integer.class, 10);

    this.spawningSpaces = new HashSet<Location>(this.parent.config().getCoordinates("moorhuhn.spawning_spaces", this.world));
    this.despawningSpaces = new HashSet<Location>(this.parent.config().getCoordinates("moorhuhn.despawning_spaces", this.world));

    this.parent.getLogger().info(spawningSpaces.size() +  " spawning spaces");
    // for(Location spawning : this.spawningSpaces)
    // {
    //   this.parent.getLogger().info("  " + spawning.getX() + ", " + spawning.getY() + ", " + spawning.getZ() + ", " + spawning.getWorld().getName());
    // }

    String estr = this.parent.config().getValue("moorhuhn.enabled", String.class, "false");
    this.enabled = true;//(estr.toLowerCase() == "true");
    this.parent.getLogger().info("Moorhuhn is " + (enabled ? "en" : "dis") + "abled");

    Vector tp_to = this.parent.config().getVectorValue("moorhuhn.tp_to", new Vector(0,0,0));
    this.tp_to = new Location(world, tp_to.getX(), tp_to.getY(), tp_to.getZ());

    Room.Config rcfg = this.parent.config().getRoomConfig("moorhuhn", this.world);
    room = new Room(this.parent, rcfg, "moorhuhn");
    room.eventHandler = this;
  }

  public void start()
  {
    if(run)
    {
      return;
    }
    this.parent.getLogger().info("Starting Moorhuhn");
    run = true;

    for(LivingEntity ent : world.getLivingEntities())
    {
      if(ent.getScoreboardTags().contains("moorhuhn_chicken"))
      {
        ent.remove();
      }
    }
  }

  public void stop()
  {
    this.parent.getLogger().info("Stopping Moorhuhn");
    run = false;
    clearChickens();
  }

  private void clearChickens()
  {
    chickens.clear();
  }

  @Override
  public void tick()
  {
    if(!run || !enabled)
    {
      return;
    }
    checkDespawn();

    HashSet<UUID> toDelete = new HashSet<>();
    chickens.forEach((key, chicken) ->
    {
      if(chicken.isDead())
      {
        toDelete.add(key);
      }
    });

    //cleanup
    for(UUID key : toDelete)
    {
      chickens.remove(key);
    }

    for(int i = 0; i < maxChickens - chickens.size(); ++i)
    {
      if(Game.rand.nextInt(3) == 0) {
        spawnChicken();
      }
    }
  }

  public void entityHit(Player player, Entity entity)
  {
    if(chickens.containsKey(entity.getUniqueId()))
    {
      // chickens.get(entity.getUniqueId()).hit();
      Location chickenLocation = entity.getLocation();
      this.world.createExplosion(chickenLocation,1.0f, false, false);
      entity.remove();
      if(!room.inRoom(player))
      {
        return;
      }
      int score = this.parent.getScoreManager().addToScore(scoreboardObjective, player, 1);
      double percent = ((double)score)/((double)neededHits);
      percent = Math.min(1.0, Math.max(0.0, percent));
      BossBar bb = bossbars.get(player.getUniqueId());
      if(bb != null)
      {
        bb.setProgress(percent);
      }
      if(score >= neededHits)
      {
        player.teleport(this.tp_to);
        room.tagPlayer(player, false);
        this.parent.getScoreManager().setScore(scoreboardObjective, player, 0);
      }
    }
  }

  private void checkDespawn()
  {
    for(Chicken chick : this.chickens.values())
    {
      for(Location despawnLoc : this.despawningSpaces)
      {
        if(!chick.isDead())
        {
          if(Utils.hasLocation(chick.getLocation(), despawnLoc, 1.0))
          {
            chick.remove();
            return;
          }
        }
      }
    }
  }

  private void spawnChicken()
  {
    Location spawn = getRandomElement(this.spawningSpaces);
    if(spawn == null) {
      parent.getLogger().warning("Spawn is null");
      return;
    }
    if(spawn.getWorld() == null) {
      parent.getLogger().warning("Spawn world is null");
      return;
    }
    Chicken chick = (Chicken) spawn.getWorld().spawnEntity(spawn, EntityType.CHICKEN);
    if(chick == null) {
      return;
    }
    chick.setInvulnerable(true);
    chick.addScoreboardTag("moorhuhn_chicken");
    this.chickens.put(chick.getUniqueId(), chick);
  }

  private <E>
  E getRandomElement(Set<? extends E> set)
  {
    // nextInt -> bound is inclusive
    int randIdx = Game.rand.nextInt(set.size()+1)+1;
    Iterator<? extends E> iterator = set.iterator();

    E randomElement = null;

    for(int i=0; i != randIdx && iterator.hasNext(); ++i)
    {
      randomElement = iterator.next();
    }
    if(randomElement == null)
    {
      parent.getLogger().warning("Spawning is null, randIdx was " + randIdx + ", set size: " + set.size());
      randomElement = set.iterator().next();
    }
    return randomElement;
  }

  public void refillSnowballs() {
//        for(Player p : this.players) {
//            System.out.println("Refill player");
//            System.out.println(p.getDisplayName());
//            p.getInventory().setItemInMainHand(null);
//            p.getInventory().setItemInMainHand(new ItemStack(Material.SNOWBALL, 16));
//        }
  }

  @Override
  public void playerEntered(Player player) {
    // finished = false;
    // this.parent.updateBossBar(0);
    // this.startTime = new Date();
    // this.parent.getBossBar().addPlayer(player);
    // this.parent.getScoreManager().resetScore("@all");
    // this.parent.getScoreManager().showScore();
    this.parent.getLogger().info("Player " + player.getDisplayName() + " entered moorhuhn");
    this.parent.getScoreManager().setScore(scoreboardObjective, player, 0);
    this.players.add(player);
    KeyedBossBar bb =  this.parent.getServer().createBossBar(new NamespacedKey(parent, player.getUniqueId().toString()), "Score", BarColor.BLUE, BarStyle.SOLID);
    bb.setProgress(0);
    bb.addPlayer(player);
    this.bossbars.put(player.getUniqueId(), bb);
    player.getInventory().setItemInMainHand(new ItemStack(Material.SNOWBALL, 16));
  }

  @Override
  public void playerLeft(Player player) {
    // this.startTime = null;
    this.parent.getLogger().info("Player " + player.getDisplayName() + " left moorhuhn");
    KeyedBossBar bb = bossbars.get(player.getUniqueId());
    if(bb != null)
    {
      bb.removeAll();
      parent.getServer().removeBossBar(bb.getKey());
    }
    this.bossbars.remove(player.getUniqueId());
    player.getInventory().setItemInMainHand(null);
    this.players.remove(player);
  }

  @Override
  public Room getRoom()
  {
    return room;
  }
}
