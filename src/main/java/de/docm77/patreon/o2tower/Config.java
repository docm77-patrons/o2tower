// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower;

import de.docm77.patreon.o2tower.utils.TpVector;
import de.docm77.patreon.o2tower.utils.Utils;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Config
{
  private final O2Tower parent;
  private FileConfiguration config;
  private String objectiveName = "Score";
  private String bossbarName = "Time Left";
  private AtomicInteger time = new AtomicInteger(90);

  public Config(O2Tower parent)
  {
    this.config = parent.getConfig();
    this.parent = parent;
  }

  public void load()
  {
    System.out.println("loading cfg");
    this.config = parent.getConfig();
    time.set(loadValue("time", Integer.class, time.get()));

    objectiveName = loadValue("mainObjectiveName", String.class, objectiveName);
    bossbarName = loadValue("bossbarName", String.class, bossbarName);
  }

  public Room.Config getRoomConfig(String roomname, World world)
  {
    Room.Config result = new Room.Config();

    result.entry = getCoordinates(roomname + ".room.entry", world);
    result.exit = getCoordinates(roomname + ".room.exit", world);
    result.playerTag = loadValue(roomname + ".room.player_tag", String.class, "");

    return result;
  }

  public String getObjectiveName() { return objectiveName; }
  public String getBossbarName() { return bossbarName; }
  public int getTime() { return time.get(); }

  public ArrayList<Location> getCoordinates(String path, World world)
  {
    ArrayList<Location> result = new ArrayList<>();
    List<String> locList = config.getStringList(path);
    for(String loc : locList)
    {
      result.add(Utils.vecToLocation(Utils.strToVector(loc), world));
    }
    return result;
  }

  public TpVector getVectorValue(String path, Vector def)
  {
    String tmp = getValue(path, String.class, "-");
    if(tmp == "-") {
      return new TpVector(def, null);
    }
    return Utils.strToVector(tmp);
  }

  public <T> T getValue(String path, Class<T> cl, T def)
  {
    return this.loadValue(path, cl, def);
  }


  private <T> T loadValue(String path, Class<T> cl, T def) {
    Object obj = config.get(path, null);
    if (obj == null) {
      parent.getLogger().info("Config for " + path + " not found, using default value (" + def + ")");
      return def;
    }
    if (!cl.isInstance(obj)) {
      parent.getLogger().warning("Unable to cast " + path + " to " + cl + ", returning default value");
      return def;
    }
    return cl.cast(obj);
  }
}
