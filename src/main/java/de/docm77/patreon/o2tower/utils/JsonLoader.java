// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.utils;

import com.google.gson.*;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class JsonLoader
{
  public static HashMap<String, HashSet<Location>> readRoom(String filepath, World world) {
    HashMap<String, HashSet<Location>> result = new HashMap<>();
    JsonParser parser = new JsonParser();
    try (FileReader reader = new FileReader(filepath)) {
      Object obj = parser.parse(reader);

      JsonArray borderList = (JsonArray) obj;

      borderList.forEach((element) -> {
        JsonObject jsonObject = (JsonObject) element;
        String name = jsonObject.get("name").getAsString();
        JsonArray coordinates = jsonObject.get("coordinates").getAsJsonArray();
        HashSet<Location> locations = new HashSet<>();
        coordinates.forEach((cel) -> {
          String coordStr = cel.getAsString();
          Vector vec = Utils.strToVector(coordStr);
          locations.add(new org.bukkit.Location(world, vec.getX(), vec.getY(), vec.getZ()));
        });
        result.put(name, locations);
      });

    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (JsonParseException e) {
      e.printStackTrace();
    }
    return result;
  }

  static private <T> T getValue(JsonObject object, String name, Class<T> cl, T def) {
    Object obj = object.get(name);
    return cl.cast(obj);
  }
}

