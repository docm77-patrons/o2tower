// Copyright (C) 2022 tinnuadan

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.docm77.patreon.o2tower.commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.docm77.patreon.o2tower.Config;
import de.docm77.patreon.o2tower.O2Tower;
import de.docm77.patreon.o2tower.utils.TpVector;
import de.docm77.patreon.o2tower.utils.Utils;

public class Stuck implements CommandExecutor
{
  final private TpVector tp_to;
  final private World world;

  public Stuck(Config cfg, World world)
  {
    TpVector tp_to = cfg.getVectorValue("stuck.tp_to", new Vector(0, 0, 0));
    this.tp_to = tp_to;
    this.world = world;
    O2Tower.instance().getLogger().info("Stuck.tp_to " + tp_to.getX() + "," + tp_to.getY() + "," + tp_to.getZ() + "," + tp_to.getFacingRotation());
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(!(sender instanceof Player))
    {
      return false;
    }
    Player player = (Player) sender;
    Location location = Utils.vecToLocation(this.tp_to, world);
    location.setYaw(this.tp_to.getFacingRotation(player));
    O2Tower.instance().getLogger().info("Stuck.tp_to " + location.getX() + "," + location.getY() + "," + location.getZ() + "," + location.getYaw());
    player.teleport(location);    
    return true;
  }

}
